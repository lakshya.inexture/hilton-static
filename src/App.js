import React from "react";
import Navbar from "./components/Navbar";
import HotelFilter from "./components/HotelFilter";
import SpecialOffers from "./components/SpecialOffers";
import HotelCarousel from "./components/HotelCarousel";
import HotelLocation from "./components/HotelLocation";
import ExperiencePackage from "./components/ExperiencePackage";
import HotelFeatures from "./components/HotelFeatures";

import "./App.css";
import ExploreHotel from "./components/ExploreHotel";
import Footer from "./components/Footer";

function App() {
    return (
        <div className="content-wrapper">
            <Navbar />
            <HotelFilter />
            <SpecialOffers />
            <HotelCarousel />
            <HotelLocation />
            <ExperiencePackage />
            <HotelFeatures />
            <ExploreHotel />
            <Footer />
        </div>
    );
}

export default App;
