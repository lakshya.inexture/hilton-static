import React from "react";
import CardHeadingAndText from "./CardHeadingAndText";
import HotelLocationCard from "./HotelLocationCard";

import hotelLocation1 from "../assets/images/hotel-location-1.jpg";
import hotelLocation2 from "../assets/images/hotel-location-2.jpg";
import hotelLocation3 from "../assets/images/hotel-location-3.jpg";
import hotelLocation4 from "../assets/images/hotel-location-4.jpg";

const HotelLocation = () => {
    return (
        <div className="container-fluid hotel-location-wrapper">
            <CardHeadingAndText
                cardHeading={"Explore Somewhere New"}
                cardText={"Unlock new memories at our hotels around the world"}
            />
            <div className="row">
                <HotelLocationCard
                    hotelLocation={"NEWARK"}
                    hotelLocationImage={hotelLocation1}
                />
                <HotelLocationCard
                    hotelLocation={"LONG BEACH"}
                    hotelLocationImage={hotelLocation2}
                />
                <HotelLocationCard
                    hotelLocation={"CARLSBAD BEACH"}
                    hotelLocationImage={hotelLocation3}
                />
                <HotelLocationCard
                    hotelLocation={"ARUBA"}
                    hotelLocationImage={hotelLocation4}
                />
            </div>
        </div>
    );
};

export default HotelLocation;
