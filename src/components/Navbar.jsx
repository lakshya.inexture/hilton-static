import React from "react";
import navLogo from "../assets/images/HiltonHotelsLogo.svg.png";

const Navbar = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light">
                <div className="container-fluid">
                    <button className="navbar-brand link-button" href="#">
                        <img
                            className="nav-img"
                            src={navLogo}
                            alt="Hilton Hotels Logos"
                        />
                    </button>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div
                        className="collapse navbar-collapse"
                        id="navbarSupportedContent"
                    >
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <button className="nav-link active link-button navbar-link">
                                    Locations
                                </button>
                            </li>
                            <li className="nav-item">
                                <button className="nav-link active link-button navbar-link">
                                    Offers
                                </button>
                            </li>
                            <li className="nav-item">
                                <button className="nav-link active link-button navbar-link">
                                    Meetings & Events
                                </button>
                            </li>
                            <li className="nav-item">
                                <button className="nav-link active link-button navbar-link">
                                    Resorts
                                </button>
                            </li>
                        </ul>
                        <button className="active link-button navbar-link">
                            Join
                        </button>
                        <span className="vertical-line"></span>
                        <button className="active link-button navbar-link">
                            Sign In<i className="fas fa-user sign-in-icon"></i>
                        </button>
                    </div>
                </div>
            </nav>
            <hr />
        </div>
    );
};

export default Navbar;
