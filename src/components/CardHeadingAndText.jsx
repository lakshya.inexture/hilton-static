import React from "react";

const CardHeadingAndText = ({ cardHeading, cardText }) => {
    return (
        <div>
            <h1 className="card-heading">{cardHeading}</h1>
            <p className="card-text">{cardText}</p>
        </div>
    );
};

export default CardHeadingAndText;
