import React from "react";
// import experienceImage from "../assets/images/experience-1.jpg";

const ExperiencePackageCard = ({ packageTitle, experienceImage }) => {
    return (
        <div className="card col-lg-4 experience-package-card-wrapper">
            <img
                src={experienceImage}
                className="card-img hotel-location-img img-fluid"
                alt="Stock-New-York-Img"
            />
            <div className="card-img-overlay card-img">
                <h3 className="card-title">{packageTitle}</h3>
            </div>
        </div>
    );
};

export default ExperiencePackageCard;
