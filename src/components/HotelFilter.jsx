import React from "react";
import SelectDate from "./SelectDate";

const HotelFilter = () => {
    return (
        <div className="container-fluid">
            <div className="mb-3 filter-content">
                <div className="location">
                    <small>Where to?</small>
                    <input
                        className="destination"
                        type="text"
                        placeholder="City, state, location, or airport"
                    />
                </div>
                <div className="start-date">
                    <SelectDate date="3" day="Thu" month="Mar" />
                </div>
                <span className="vertical-line mt-3"></span>
                <div className="end-date">
                    <SelectDate date="4" day="Fri" month="Mar" />
                </div>
                <div className="room-type">
                    <button className="btn btn-outline-dark">
                        1 room, 1 guest
                    </button>
                </div>
                <div className="special-rates">
                    <button className="btn btn-outline-dark">
                        Special rates
                    </button>
                </div>
                <div className="find-hotel">
                    <button className="btn find-hotel-btn">Find a hotel</button>
                </div>
            </div>
        </div>
    );
};

export default HotelFilter;
