import React from "react";
import hotelFeatures from "../assets/images/hotel-features-1.jpg";
import CardHeadingAndText from "./CardHeadingAndText";

const HotelFeatures = () => {
    const featureContent = [
        {
            title: "Technology & Innovation",
            src: hotelFeatures,
            heading: "Pushing the Envelope",
            content:
                "With more personalized, digital services, every stay feels unique to you and your preferences",
        },
        {
            title: "Food & Beverage",
            src: hotelFeatures,
            heading: "Pushing the Envelope",
            content:
                "With more personalized, digital services, every stay feels unique to you and your preferences",
        },
        {
            title: "Innovation Meeting",
            src: hotelFeatures,
            heading: "Pushing the Envelope",
            content:
                "With more personalized, digital services, every stay feels unique to you and your preferences",
        },
        {
            title: "Our Hotels",
            src: hotelFeatures,
            heading: "Pushing the Envelope",
            content:
                "With more personalized, digital services, every stay feels unique to you and your preferences",
        },
        {
            title: "Join Hilton Honors",
            src: hotelFeatures,
            heading: "Pushing the Envelope",
            content:
                "With more personalized, digital services, every stay feels unique to you and your preferences",
        },
    ];
    return (
        <div className="container-fluid hotel-features">
            <ul className="nav nav-tabs features-title">
                {featureContent.map((e, ind) => {
                    return (
                        <>
                            <li className="nav-item" key={ind}>
                                <button
                                    className={`nav-link tabs-btn link-button ${ind ===
                                        0 && "active"}`}
                                    data-bs-toggle="tab"
                                    data-bs-target={ind}
                                    type="button"
                                    role="tab"
                                >
                                    {e.title}
                                </button>
                            </li>
                            {ind < featureContent.length - 1 && (
                                <span className="vertical-line"></span>
                            )}
                        </>
                    );
                })}
            </ul>
            <div className="tab-content container-fluid">
                {featureContent.map((e, ind) => {
                    return (
                        <div
                            className={`tab-pane fade ${ind === 0 &&
                                "show active"}`}
                            role="tabpanel"
                            id={ind}
                            key={ind}
                        >
                            <div className="row">
                                <div className="col-lg-7 feature-img">
                                    <img src={e.src} alt={e.title} />
                                </div>
                                <div className="col-lg-5 feature-text">
                                    <CardHeadingAndText
                                        cardHeading={e.heading}
                                        cardText={e.content}
                                    />
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default HotelFeatures;
