import React from "react";
import CardHeadingAndText from "./CardHeadingAndText";
import ExperiencePackageCard from "./ExperiencePackageCard";

import experience_1 from "../assets/images/experience-1.jpg";
import experience_2 from "../assets/images/experience-2.jpg";
import experience_3 from "../assets/images/experience-3.jpg";
import experience_4 from "../assets/images/experience-4.jpg";
import experience_5 from "../assets/images/experience-5.jpg";
import experience_6 from "../assets/images/experience-6.jpg";

const ExperiencePackage = () => {
    return (
        <div className="container-fluid experience-package-wrapper">
            <CardHeadingAndText
                cardHeading={"Experience Something New"}
                cardText={
                    "Make your upcoming travel memorable with exciting experiences and offers from Hilton"
                }
            />
            <div className="row">
                <ExperiencePackageCard
                    packageTitle={"2X POINTS PACKAGE"}
                    experienceImage={experience_1}
                />
                <ExperiencePackageCard
                    packageTitle={"BREAKFAST INCLUDED PACKAGE"}
                    experienceImage={experience_2}
                />
                <ExperiencePackageCard
                    packageTitle={"EARN 70K POINTS + MORE"}
                    experienceImage={experience_3}
                />
                <ExperiencePackageCard
                    packageTitle={"SEE WHERE YOUR POINTS CAN TAKE YOU"}
                    experienceImage={experience_4}
                />
                <ExperiencePackageCard
                    packageTitle={"ANOTHER NIGHT ON US"}
                    experienceImage={experience_5}
                />
                <ExperiencePackageCard
                    packageTitle={"EARN 2,500 POINTS WITHOUT LIMITS"}
                    experienceImage={experience_6}
                />
            </div>
        </div>
    );
};

export default ExperiencePackage;
