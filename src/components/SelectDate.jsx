import React from "react";

const SelectDate = ({ date, day, month }) => {
    return (
        <div className="select-date">
            <span className="date">{date}</span>
            <p className="day">{day}</p>
            <p className="month">{month}</p>
        </div>
    );
};

export default SelectDate;
