import React, { useState } from "react";

const SpecialOffers = () => {
    const [getOffer, setGetOffer] = useState(
        `Get to free nights faster! Earn 70k bonus points + a Free Night Reward. Terms apply.`
    );
    const [pageOfferNumber, setPageOfferNumber] = useState(1);
    const [totalOffers, setTotalOffers] = useState(2);
    return (
        <div className="container-fluid special-offer">
            <h5>
                {getOffer}
                <small className="special-offer-terms">
                    <button className="link-button">Learn More.</button>
                </small>
            </h5>
            <div className="offer-pagination">
                <button className="link-button">&#9001;</button>
                <span>
                    {pageOfferNumber} of {totalOffers}
                </span>
                <button className="link-button">&#9002;</button>
            </div>
        </div>
    );
};

export default SpecialOffers;
