import React from "react";
import exploreBg from "../assets/images/explore-background.jpg";
import CardHeadingAndText from "./CardHeadingAndText";

const ExploreHotel = () => {
    return (
        <div className="container-fluid explore-wrapper">
            <div className="explore-img-bg">
                <img
                    src={exploreBg}
                    alt="alley-bg"
                    className="img-fluid explore-img"
                />
            </div>
            <div className="container explore-content">
                <CardHeadingAndText
                    cardHeading={"Plan Your All-Inclusive Escape"}
                    cardText={
                        "Discover inspiring destinations and learn why All-Inclusive is a great option for your next adventure."
                    }
                />
                <button className="btn explore-btn">
                    Explore All-Inclusive Resorts
                </button>
            </div>
            <hr />
        </div>
    );
};

export default ExploreHotel;
