import React from "react";

const Footer = () => {
    return (
        <div className="container-fluid footer">
            <div className="row">
                <div className="contact col-lg-8">
                    <p>How can we help?</p>
                    <p className="hotel-number">1-800-HILTONS</p>
                    <small>Call us. It's toll-free.</small>
                    <div className="social-media">
                        <i className="fab fa-twitter twitter-icon"></i>
                        <i className="fab fa-facebook-square facebook-icon"></i>
                        <i className="fab fa-instagram instagram-icon"></i>
                        <i className="fab fa-youtube youtube-icon"></i>
                        <i className="fab fa-pinterest pinterest-icon"></i>
                    </div>
                </div>
                <div className="col-lg-2 offset-lg">
                    <ul>
                        <li className="footer-link">Travel Inspiration</li>
                        <li className="footer-link">Pet-Friendly Stays</li>
                        <li className="footer-link">Hilton Gift Card</li>
                        <li className="footer-link">
                            Global Privacy Statement
                        </li>
                        <li className="footer-link">Site Map</li>
                        <li className="footer-link">Careers</li>
                        <li className="footer-link">Development</li>
                        <li className="footer-link">Media</li>
                        <li className="footer-link">Web Accessibility</li>
                    </ul>
                </div>
                <div className="col-lg-2 offset-lg">
                    <ul>
                        <li className="footer-link">Customer Support</li>
                        <li className="footer-link">Cookies Statement</li>
                        <li className="footer-link">Site Usage Agreement</li>
                        <li className="footer-link">Personal Data Requests</li>
                        <li className="footer-link">
                            Do Not Sell My Personal Information
                        </li>
                        <li className="footer-link">
                            Modern Slavery and Human Trafficking
                        </li>
                        <li className="footer-link">
                            Hilton Honors Discount Terms & Conditions
                        </li>
                        <li className="footer-link">Hilton Hotline </li>
                    </ul>
                </div>
            </div>
            <small className="copyright">&#169; 2022 Hilton</small>
        </div>
    );
};

export default Footer;
