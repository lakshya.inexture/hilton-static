import React from "react";
import carousel1 from "../assets/images/hotel-carousel-1.jpg";
import carousel2 from "../assets/images/hotel-carousel-2.jpg";
import carousel3 from "../assets/images/hotel-carousel-3.jpg";
import CardHeadingAndText from "./CardHeadingAndText";

const HotelCarousel = () => {
    return (
        <div className="hotel-carousel">
            <div
                id="carouselExampleIndicators"
                className="carousel slide"
                data-bs-ride="carousel"
            >
                <div className="carousel-indicators">
                    <button
                        type="button"
                        data-bs-target="#carouselExampleIndicators"
                        data-bs-slide-to="0"
                        className="active"
                        aria-current="true"
                    ></button>
                    <button
                        type="button"
                        data-bs-target="#carouselExampleIndicators"
                        data-bs-slide-to="1"
                    ></button>
                    <button
                        type="button"
                        data-bs-target="#carouselExampleIndicators"
                        data-bs-slide-to="2"
                    ></button>
                </div>
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img
                            src={carousel1}
                            className="d-block carousel-img"
                            alt="carousel-img"
                        />
                        <div className="carousel-caption carousel-overlay">
                            <p className="carousel-overlay-text">
                                Hilton Dead Sea Resort & Spa, Sweimeh, Jordan
                            </p>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img
                            src={carousel2}
                            className="d-block carousel-img"
                            alt="carousel-img"
                        />
                        <div className="carousel-caption carousel-overlay">
                            <p className="carousel-overlay-text">
                                Hilton Dead Sea Resort & Spa, Sweimeh, Jordan
                            </p>
                        </div>
                    </div>
                    <div className="carousel-item">
                        <img
                            src={carousel3}
                            className="d-block carousel-img"
                            alt="carousel-img"
                        />
                        <div className="carousel-caption carousel-overlay">
                            <p className="carousel-overlay-text">
                                Hilton Dead Sea Resort & Spa, Sweimeh, Jordan
                            </p>
                        </div>
                    </div>
                </div>
                <button
                    className="carousel-control-prev"
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="prev"
                >
                    <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button
                    className="carousel-control-next"
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="next"
                >
                    <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>
            <CardHeadingAndText
                cardHeading={"Explore the Heart of the Dead Sea"}
                cardText={
                    "Our hotel sits at the lowest land point on Earth, with direct access to the Dead Sea beachfront. Enjoy our spa facilities, rooftop bar, two infinity pools, and the area's only floating pontoon with sundeck."
                }
            />
            <button className="btn view-hotel-btn">View Hotel</button>
        </div>
    );
};

export default HotelCarousel;
