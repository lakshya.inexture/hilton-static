import React from "react";

const HotelLocationCard = ({ hotelLocationImage, hotelLocation }) => {
    return (
        <div className="card text-white col-lg-3 hotel-location-card-wrapper">
            <img
                src={hotelLocationImage}
                className="card-img hotel-location-img img-fluid"
                alt="Stock-New-York-Img"
            />
            <div className="card-img-overlay card-img">
                <h3 className="card-title">{hotelLocation}</h3>
            </div>
        </div>
    );
};

export default HotelLocationCard;
